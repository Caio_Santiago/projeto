﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class DBZdatabase
    {
        public int SALVAR(DBZdto dto)

        {
            string script =
                @"INSERT INTO DBZStoreDB (nm_produto, vl_preco)
                 VALUES (@nm_produto, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.produto));
            parms.Add(new MySqlParameter("vl_preco", dto.preco));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public List<DBZdto> Consultar(string produto, decimal preco)
        {
            string script =
            @"SELECT * 
                FROM tb_produto 
               WHERE nm_produto like @nm_produto 
                 AND vl_preco like @vl_preco";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + produto + "%"));
            parms.Add(new MySqlParameter("vl_preco", "%" +  preco + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DBZdto> produtos = new List<DBZdto>();

            while (reader.Read())
            {
                DBZdto novoproduto = new DBZdto();
                novoproduto.id = reader.GetInt32("id_produto");
                novoproduto.produto = reader.GetString("nm_produto");
                novoproduto.preco = reader.GetString("vl_preco");

                produtos.Add(novoproduto);
            }
            reader.Close();

            return produtos;

        }
    }
}
