﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoConsultar : UserControl
    {
        public frmProdutoConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        public void CarregarGrid()
        {
            try
            {
                string produto = txtProduto.Text;
                

                DBZbusiness business = new DBZbusiness();
                //List<DBZdto> produtos = business.Consultar(produto);

                dgvProdutos.AutoGenerateColumns = false;
                //dgvProdutos.DataSource = produtos;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao salvar o produto: " + ex.Message, "Nsf Jam",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
            }
        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
