﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoCadastrar : UserControl
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            DBZdto dto = new DBZdto();
            dto.produto = txtProduto.Text;
            dto.preco = txtPreco.Text;
            DBZbusiness business = new DBZbusiness();
            business.SALVAR(dto);
            MessageBox.Show("O produto foi salvo com Sucesso. !!!");
        }
    }
}
